import createLensAPI, { LensAPIOptions } from './source/lens-api';
import {
    LensRequestTargetType, 
    LensRef,
    LensAuthRequest,
    LensRequest, 
    LensRequestData,
    LensRequestContainer,
    LensRequestItem,
    LensRequestDataPreselectModeType,
    LensRequestContainerDirectionType,
    LensRequestResult,
    LoginRequest,
    LoginRequestResult,
    LoginScreenType,
    AuthorizeRequest,
    AuthorizeRequestResult,
    UserInfo, 
    User,
    GetLensType,
    GetLensesType
} from './source/types';
import Lens from './source/lens';
import LensData from './source/lens-data';

let win = null; 
if(typeof window !== 'undefined') {
    win = window; 
} 
let ffetch = null; 
if(typeof fetch !== 'undefined') {
    ffetch = (...props) => { return fetch.apply(win, props); }; 
} 


export default createLensAPI({ window: win, fetch: ffetch });
export {
    LensAPIOptions,
    Lens,
    LensRef,
    LensAuthRequest,
    LensRequest,
    LensRequestData,
    LensRequestContainer,
    LensRequestItem,
    LensRequestDataPreselectModeType,
    LensRequestContainerDirectionType,
    LensRequestTargetType,
    LensData,
    LensRequestResult,
    LoginRequest,
    LoginRequestResult,
    LoginScreenType,
    AuthorizeRequest,
    AuthorizeRequestResult,
    UserInfo,
    User,
    GetLensType,
    GetLensesType
}