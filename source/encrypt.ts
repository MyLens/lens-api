import * as crypto from 'crypto';
import Elliptic from 'elliptic';

const ecurve = new Elliptic.ec('secp256k1');


/*
    Encryption process originally pulled from blockstack.
    If we reference it directly, it would be fragile; things would
    break if they move it since they don't export it directly, and
    the user may not be logged in (and shouldn't have to be) in order
    to use it.
    As we transition from using the blockstack library to not using it,
    we don't want to break reverse compatibility of encrypted lenses
    so we want to maintain the same encryption algorithm.
*/
function aes256CbcDecrypt(iv, key, ciphertext) {
    const cipher = crypto.createDecipheriv('aes-256-cbc', key, iv);
    return Buffer.concat([cipher.update(ciphertext), cipher.final()]);
}
function hmacSha256(key, content) {
    return crypto.createHmac('sha256', key).update(content).digest();
}
function equalConstTime(b1, b2) {
    if (b1.length !== b2.length) {
        return false;
    }
    let res = 0;
    for (let i = 0; i < b1.length; i++) {
        res |= b1[i] ^ b2[i]; // jshint ignore:line
    }
    return res === 0;
}
function sharedSecretToKeys(sharedSecret) {
    // generate mac and encryption key from shared secret
    const hashedSecret = crypto.createHash('sha512').update(sharedSecret).digest();
    return {
        encryptionKey: hashedSecret.slice(0, 32),
        hmacKey: hashedSecret.slice(32)
    };
}   
function getHexFromBN(bnInput) {
    const hexOut = bnInput.toString('hex');
    if (hexOut.length === 64) {
        return hexOut;
    }
    else if (hexOut.length < 64) {
        // pad with leading zeros
        // the padStart function would require node 9
        const padding = '0'.repeat(64 - hexOut.length);
        return `${padding}${hexOut}`;
    }
    else {
        throw new Error('Generated a > 32-byte BN for encryption. Failing.');
    }
}

export interface EncryptedType {
    iv: string,
    ephemeralPK: string,
    cipherText: string,
    mac: string
}

export function decrypt(privateKey: string, cipherObject: EncryptedType) : string {
    const ecSK = ecurve.keyFromPrivate(privateKey, 'hex');
    const ephemeralPK = ecurve.keyFromPublic(cipherObject.ephemeralPK, 'hex').getPublic();
    const sharedSecret = ecSK.derive(ephemeralPK);
    const sharedSecretBuffer = Buffer.from(getHexFromBN(sharedSecret), 'hex');
    const sharedKeys = sharedSecretToKeys(sharedSecretBuffer);
    const ivBuffer = Buffer.from(cipherObject.iv, 'hex');
    const cipherTextBuffer = Buffer.from(cipherObject.cipherText, 'hex');
    const macData = Buffer.concat([ivBuffer,
        Buffer.from(ephemeralPK.encodeCompressed()),
        cipherTextBuffer]);
    const actualMac = hmacSha256(sharedKeys.hmacKey, macData);
    const expectedMac = Buffer.from(cipherObject.mac, 'hex');
    if (!equalConstTime(expectedMac, actualMac)) {
        throw new Error('Decryption failed: failure in MAC check');
    }
    const plainText = aes256CbcDecrypt(ivBuffer, sharedKeys.encryptionKey, cipherTextBuffer);
    return plainText.toString();
}