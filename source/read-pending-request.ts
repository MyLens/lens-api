import BrowserWindow from './browser-window';
import UrlParams from './url-params';
import { LensRequestResult } from './types';

export interface RequestError {
    error: string,
    description?: string,
    uri?: string
}

export interface PendingRequest {
    result?: {
        code: string,
        lensResult?: LensRequestResult 
    },
    error?: RequestError
    state: string,
}

export default function readPendingRequest(window: BrowserWindow) : PendingRequest | null {    
    const params = new UrlParams(window.location.href);
    let pendingRequest: PendingRequest | null = null;

    // Everyone needs a state
    if(params.paramExists('state')) {

        // Minimal requirements for valid response
        if(params.paramExists('code')) {
            pendingRequest = {
                state: params.getParam('state'),
                result: {
                    code: params.getParam('code')
                }
            }

            // Deal with a lens being returned.
            if(params.paramExists('url') && params.paramExists('lensId')) {
                const result : LensRequestResult = {
                    id: params.getParam('lensId')!,
                    lens: {
                        url: params.getParam('url')!
                    }
                } 

                // Add optional
                if(params.paramExists('userId') && params.paramExists('userPublicKey') && params.paramExists('userPrivateKey')) {
                    result.user = {
                        id: params.getParam('userId')!,
                        publicKey: params.getParam('userPublicKey')!,
                        privateKey: params.getParam('userPrivateKey')!
                    }
                    // Assign the private key if we received one, so the lensRef
                    // is a full ref.
                    result.lens.privateKey = result.user.privateKey;
                }
                if(params.paramExists('lensState')) result.state = params.getParam('lensState')!;

                pendingRequest.result.lensResult = result;
            } 
        } 
        // Check for error.
        else if(params.paramExists('error')) {
            const error: RequestError = {
                error: params.getParam('error')!
            };
            
            // Add optional
            if(params.paramExists('error_description')) error.description = params.getParam('error_description')!;
            if(params.paramExists('error_uri')) error.uri = params.getParam('error_uri')!;

            pendingRequest = {
                state: params.getParam('state'),
                error: error
            };
        }
    }

    return pendingRequest;
}