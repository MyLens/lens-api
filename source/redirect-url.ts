import { AuthorizeRequest, LensRequestTarget } from './types';
import { stringifyScopes } from './scopeType';
import crypto from 'crypto';

function toBase64(s: string) {
    return Buffer.from(s).toString('base64');
}

function fromBase64(s: string) {
    return Buffer.from(s, 'base64').toString();
}

export default class RedirectUrl {
    private requestUrl: string;
    private clientId: string;

    constructor(requestUrl: string, clientId: string) {
        this.requestUrl = requestUrl;
        this.clientId = clientId;
    }

    private generateCodeChallenge(codeChallengeMethod?: string) : { verifier: string, challenge: string, method: string } {
        // for https://tools.ietf.org/html/rfc7636#page-17
        if(!codeChallengeMethod) codeChallengeMethod = 'S256'
        // Recommended is 32 octets
        const codeVerifier = crypto.randomBytes(32).toString('base64');
        const codeChallenge = crypto.createHash('sha256').update(codeVerifier).digest('base64'); 
        return {
            verifier: codeVerifier,
            challenge: codeChallenge,
            method: codeChallengeMethod 
        };
    }
    
    build(request: AuthorizeRequest): {
        url: string,
        codeChallengeVerifier: string
    } {

        const codeChallenge = this.generateCodeChallenge();

        let url = `${this.requestUrl}?`
            + `response_type=code`
            + `&scope=${encodeURIComponent(stringifyScopes(request.scope))}`
            + `&client_id=${encodeURIComponent(this.clientId)}`
            + `&redirect_uri=${encodeURIComponent(request.redirectUri)}`
            + `&state=${encodeURIComponent(request.state)}`
            + `&code_challenge=${encodeURIComponent(codeChallenge.challenge)}`
            + `&code_challenge_method=${encodeURIComponent(codeChallenge.method)}`
        ;

        if(request.vLoginScreen) {
            url += `&v_login_screen=${encodeURIComponent(request.vLoginScreen)}`;
        }
        if(request.uiRequestPrompt) {
            url += `&ui_request_prompt=${encodeURIComponent(request.uiRequestPrompt)}`;
        }
        if(request.uiConnectButtonLabel) {
            url += `&ui_connect_button_label=${encodeURIComponent(request.uiConnectButtonLabel)}`;
        }
        if(request.uiCancelButtonLabel) {
            url += `&ui_cancel_button_label=${encodeURIComponent(request.uiCancelButtonLabel)}`;
        }

        if(request.lensRequest) {
            const lensRequest = request.lensRequest!;
            
            if(lensRequest.id) url += `&id=${encodeURIComponent(lensRequest.id)}`;
            if(lensRequest.name) url += `&name=${encodeURIComponent(lensRequest.name)}`;
            if(lensRequest.label) url += `&label=${encodeURIComponent(lensRequest.label)}`;
        
            if(typeof lensRequest.target === 'string') {
                url += `&targetType=${encodeURIComponent(lensRequest.target)}`;
            } else if(typeof lensRequest.target === 'object') {
                const target : LensRequestTarget = lensRequest.target as LensRequestTarget;
                if(target.type) url += `&targetType=${encodeURIComponent(target.type)}`;
                if(target.id) url += `&targetId=${encodeURIComponent(target.id)}`;
                if(target.publicKey) url += `&targetPublicKey=${encodeURIComponent(target.publicKey)}`;
            }
        
            url += `&data=${encodeURIComponent(toBase64(JSON.stringify(lensRequest.data)))}`;
            if(lensRequest.state) url += `&lensState=${encodeURIComponent(lensRequest.state)}`;
        }

        return { url, codeChallengeVerifier: codeChallenge.verifier };
    }
}
