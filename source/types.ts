import { ResolvedLens } from '@mylens/lens-resolver';

import ScopeType from './scopeType';

export enum LensRequestTargetType {
    Self = "self",
    General = "general"
}

export interface LensRequestTarget {
    type: LensRequestTargetType,
    id?: string,
    publicKey?: string
}

export enum LensRequestDataPreselectModeType {
    Existing = "existing",
    New = "new",
}

export enum ImageEncodingType {
    PNG = "png",
    JPEG = "jpeg"
}

export enum LensRequestContainerDirectionType {
    Horizontal='horizontal',
    Veritcal='vertical'
}

export interface LensRequestData {
    name: string,
    type?: string,
    label?: string,
    value?: string,
    display?: string,
    validators?: Array<string | Validator>,
    preselectMode?: LensRequestDataPreselectModeType,

    // type and display specific props
    rows?: number,
    min?: number,
    max?: number,
    options?: Array<string | number | boolean | Option>,

    imageQuality?: number,
    maxWidth?: number,
    minWidth?: number,
    maxHeight?: number,
    minHeight?: number
    ratio?: number,
    imageEncoding?: ImageEncodingType

    // Composite data types are like containers in that they
    // hold other data items.  This introduces the idea
    // of "groups". I.e you can havea  group called "emergencyContact", 
    // so that a user can easily select an entire group of data that is 
    // related to an emergency contact rather than having to select
    // field individually. 
    // They will display similar to a container.
    data?: Array<LensRequestItem>, 
    direction?: LensRequestContainerDirectionType,
}

export interface LensRequestContainer {
    data: Array<LensRequestItem>,

    label?: string,
    direction?: LensRequestContainerDirectionType,
}

export type LensRequestItem = LensRequestData | LensRequestContainer; 

export interface Option {
    value: string | number | boolean,
    label?: string
}

export interface Validator {
    type: string,
    label?: string,

    // type specific props
    test?: string,
    min?: number,
    max?: number
    options?: Array<string | number | boolean | Option>
    
    maxWidth?: number,
    minWidth?: number,
    maxHeight?: number,
    minHeight?: number
    ratio?: number,
    imageEncoding?: ImageEncodingType
}

export interface LensAuthRequest {
    id?: string,
    target?: LensRequestTarget | LensRequestTargetType,
    label?: string,
    name?: string,
    data: Array<LensRequestItem>,
    state?: string
}

export interface LensRequest extends LensAuthRequest {
    redirectUri?: string,

    vLoginScreen?: LoginScreenType,
    uiRequestPrompt?: string,
    uiConnectButtonLabel?: string,
    uiCancelButtonLabel?: string
}

export interface LensRequestResult {
    id: string,
    lens: LensRef,
    user?: User,
    state?: string
}

export interface User {
    id: string,
    publicKey: string,
    privateKey: string
}

export interface LensRef {
    url: string,
    privateKey?: string
}


export enum LoginScreenType {
    Login='login',
    Signup='signup'
}


export interface LoginRequest {
    redirectUri: string,
    scope?: Array<ScopeType>,
    state?: string,

    vLoginScreen?: LoginScreenType,
    uiRequestPrompt?: string,
    uiConnectButtonLabel?: string,
    uiCancelButtonLabel?: string
}

export interface LoginRequestResult {
    accessToken: string,
    idToken: string,
    expires: Date,
    scope: Array<ScopeType>,
    state?: string
}


export interface AuthorizeRequest extends LoginRequest {
    lensRequest?: LensAuthRequest
}

export interface AuthorizeRequestResult extends LoginRequestResult {
    lensRequest?: LensRequestResult
}

export interface UserInfo {
    userId: string, 
    preferredUsername?: string
}

type GetLensType = (url: string, decrypt: (encryptedContent: string) => string) => Promise<ResolvedLens>
export {GetLensType}

type GetLensesType = (requests: Array<{ url: string, decrypt: (encryptedContent: string) => string }>) => Promise<Array<ResolvedLens|null>>
export {GetLensesType}