import {
    AuthorizeRequest, 
    LensRequestItem,
    LensRequestData, 
    LensRequestTargetType, 
    LensRequestTarget,
    Validator,
    Option
} from './types';
import { ScopeTypes, unionScopes, DefaultScopes } from './scopeType';
import crypto from 'crypto';


// Map types to possible displays
const restrictions = [
    {
        type: 'string',
        displays: ['text', 'textarea', 'select']
    },
    {
        type: 'number',
        displays: ['number', 'select']
    },
    {
        type: 'boolean',
        displays: ['checkbox', 'radio', 'select']
    },
    {
        type: 'string/email-address',
        displays: ['email-address']
    },
    {
        type: 'string/phone-number',
        displays: ['phone-number']
    },
    {
        type: 'number/integer',
        displays: ['integer']
    },
    {
        type: 'composite', 
        displays: ['composite']
    }
];

const validators = [
    "required", 
    "length", 
    "range", 
    "regex", 
    "options", 
    "imageWidth", 
    "imageHeight", 
    "imageRatio",
    "fileSize",
    "imageEncoding"
];



/* Validate (and normalize) any passed in lens requests before use. */

export default class LensValidator {

    normalize(request: AuthorizeRequest) : AuthorizeRequest {
        // Copy it
        request = Object.assign({}, {
            // Default values
        }, request, {
            scope: unionScopes(request.scope, DefaultScopes),
            // Make sure a state exists.
            state: typeof request.state !== 'undefined' 
                ? request.state 
                : crypto.randomBytes(32).toString('base64')
        });

        return request;
    }


    private determineType(data: LensRequestData) {
        // If type is given, use that
        if(data.type) return data.type;

        // Barring any other info, treat it as a string
        return 'string';
    }

    validate(request: AuthorizeRequest) : void {
        const is = (p, type) => { return typeof p === type; }; 

        // Validation

        // redirectUri is necessary
        if(!request.redirectUri) throw new Error("Missing property 'redirectUri'.");
        // RedirectUri may not have a fragment.
        const fragmentIndex = request.redirectUri.indexOf('#');
        if(fragmentIndex > -1) throw new Error(`Property 'redirectUri' contains a fragment at position ${fragmentIndex}. Fragments are not permitted on the 'redirectUri'.`)
        // Scope is required
        if(!request.scope || request.scope.length == 0) throw new Error("Missing property 'scope'.");
        // Scope must be valid.
        const invalidScope = request.scope.find(s => !ScopeTypes.includes(s));
        if(invalidScope) throw new Error(`Property 'scope' contains an invalid scope '${invalidScope}'. Acceptable values are '${ScopeTypes.length == 1 ? ScopeTypes[0] : (ScopeTypes.slice(0,-1).join("', '") +"' or '"+ ScopeTypes.slice(-1))}'.`);
        // State is necessary
        if(!request.state) throw new Error("Missing property 'state'.");

        // LensRequest must be valid if provided.
        if(request.lensRequest) {
            const lensRequest = request.lensRequest!;
            // If target type is 'self', and they included an id, that isn't quite right.
            const targetType = is(lensRequest.target, 'string')
                ? lensRequest.target as LensRequestTargetType 
                : is(lensRequest.target, 'object') 
                ? (lensRequest.target as LensRequestTarget).type
                : LensRequestTargetType.Self;
            if(targetType == LensRequestTargetType.Self 
                && (is(lensRequest.target, 'object') 
                && (lensRequest.target as LensRequestTarget).id)) throw new Error(`Providing a 'target.id' value is invalid with a 'target.type' of 'self'.`);

            const validateLensItem : (item: LensRequestItem) => void = (item) => {
                // Determine if it is a container or a data
                const data = item as LensRequestData;
                if(data.type) { 

                    const type = this.determineType(data);
                    const baseType = type.split('/')[0];

                    // Collect all possible display types for this type.
                    const restrictionsSorted = restrictions.sort((a,b) => b.type.split('/').length - a.type.split('/').length);
                    const displays = restrictionsSorted
                        .filter(r => type.startsWith(r.type))
                        .reduce((displays, r) => displays.concat(r.displays),[]);

                    // If display we want isn't in the list, then we don't know about the base type.
                    if(displays.length == 0) throw new Error(`Data named '${data.name}' has an invalid type '${type}'; base type '${baseType}' is unrecognized.`);
                    
                    // If the display is listed...
                    if(data.display) {
                        // If display we want isn't in the list, then we have a problem
                        if(displays.indexOf(data.display) == -1) throw new Error(`Data named '${data.name}' has a display '${data.display}' that is unknown for type '${type}'`);

                        // If 'select' is the display, 'options' needs to exist and be valid.
                        if(data.display == 'select') {
                            if(is(data.options, 'object')) {
                                if(data.options instanceof Array) {
                                    const badValues = data.options.filter(o => !is(o, baseType) && !is((o as Option).value, baseType));
                                    if(badValues.length > 0) throw new Error(`Data named '${data.name}' has ${badValues.length} options with incorrect value types.`);
                                } else throw new Error(`Data named '${data.name}' has an options prop that is not an Array.`);
                            } else throw new Error(`Data named '${data.name}' is missing or has an invaid options prop.`);
                        }
                    }

                    // Check validators.
                    if(data.validators) {
                        for(const oVal of data.validators) {
                            const validator = is(oVal, 'string') 
                                ? { type: oVal as string } as Validator
                                : oVal as Validator; 

                            // Make sure it is a recognized validator.
                            if(validators.indexOf(validator.type) == -1) throw new Error(`Validator '${validator.type}' on data name '${data.name}' is unrecognized.`);

                            // Take care of special required props per validator.
                            if((validator.type == 'range' || validator.type == 'length')
                                && !is(validator.max, 'number') && !is(validator.min, 'number')) throw new Error(`Validator '${validator.type}' on data name '${data.name}' requires either a 'min' or 'max' property defined as numbers.`); 

                            if(validator.type == 'regex' && !is(validator.test, 'string')) throw new Error(`Validator '${validator.type}' on data name '${data.name}' requires a 'test' property defined as a string containing a valid RegExp pattern.`); 

                            if(validator.type == 'options') {
                                // Does validator have 'options'?
                                if(is(validator.options, 'object')) {
                                    if(validator.options instanceof Array) {
                                        const badValues = validator.options.filter(o => !is(o, baseType) && !is((o as Option).value, baseType));
                                        if(badValues.length > 0) throw new Error(`Validator '${validator.type}' on data name '${data.name}' has ${badValues.length} options with incorrect value types.`);
                                    } else throw new Error(`Validator '${validator.type}' on data name '${data.name}' has an options prop that is not an Array.`);    
                                } else if(!is(data.options, 'object')) throw new Error(`Validator '${validator.type}' on data name '${data.name}' is missing or has an invalid 'options'.`);
                            }

                            if(validator.type == 'imageRatio' && !is(validator.ratio, 'number')) throw new Error(`Validator '${validator.type}' on data name '${data.name}' requires a 'ratio' property defined as a number.`);
                            if(validator.type == 'imageWidth' && !is(validator.minWidth, 'number') && !is(validator.maxWidth, 'number')) throw new Error(`Validator '${validator.type}' on data name '${data.name}' requires either a 'minWidth' or 'maxWidth' property defined as a numbers.`);
                            if(validator.type == 'imageHeight' && !is(validator.minHeight, 'number') && !is(validator.maxHeight, 'number')) throw new Error(`Validator '${validator.type}' on data name '${data.name}' requires either a 'minHeight' or 'maxHeight' property defined as a numbers.`);
                        }
                    }
                } else {
                    // Validate the container?

                    // Nothing specific to validate yet.
                }

                // Validate any children
                if(item.data) item.data.forEach(validateLensItem);
            };

            // Check data requirements
            lensRequest.data.forEach(validateLensItem);
        }
    }
}
