import { 
    Data,
    AliasMap
} from '@mylens/lens-resolver';

function copyData(data: Array<Data>) : Array<Data> {
    return data.map(d => {
        const copy = Object.assign({}, d);
        if(d.children) copy.children = copyData(d.children);
        return copy;
    });
}

function copyAliases(aliases: AliasMap) : AliasMap {
    return Object.keys(aliases).reduce((prev,key) => {
        return Object.assign({}, prev, {
            [key]: aliases[key].map(a => {
                const ret : { alias?: string, children?: AliasMap }= {};
                if(a.alias) ret.alias = a.alias;
                if(a.children) ret.children = copyAliases(a.children);
                return ret;
            })
        });
    }, {} as AliasMap);
}

export default class LensData implements Iterable<LensData> {
    private _data: Array<Data>;
    private _aliases: AliasMap;

    constructor(data: Array<Data>, aliases: AliasMap) {
        this._data = copyData(data);
        this._aliases = copyAliases(aliases);
    }


    // Iterators

    [Symbol.iterator]() {
        let cur = null;
        return {
            next: () => {
                if(cur) cur = cur.next();
                else cur = this;

                return {
                    value: cur,
                    done: cur == null
                };
            }
        };
    }

    // Indexers

    next() : LensData {
        return this.index(1); // Advance to the next index.
    }

    last() : LensData {
        return this.index(this._data.length-1);
    }

    index(i: number) : LensData {
        if(!this._data[i]) return null;

        // Grab the data we are removing.
        const removedData = this._data.slice(0, i);
        const data = this._data.slice(i);

        // Go through and build up new AliasMap that doesn't include the data we removed.
        const newAliases = copyAliases(this._aliases);
        removedData.forEach(d => {
            const dAlias = newAliases[d.id];
            // If the map has this data in it
            if(dAlias) {
                // And it is the last one, the entry can be removed.
                if(dAlias.length == 1) delete newAliases[d.id];
                else {
                    // Otherwise remove the first member in the array of aliases for this data Id.
                    newAliases[d.id].slice(1);
                }
            }
        });

        // The new LensData includes only the remaining data, and an aliasMap
        // that doesn't include the 'consumed' data.
        return new LensData(data, newAliases);
    }

    get(key?: string) : LensData | null {
        
        // If no key was provided, return a copy of this data.
        if(!key) {
            // Copies of these structures will be made when data is created.
            return new LensData(this._data, this._aliases);
        }

        // Find our key in the aliases
        const matches = this._data.reduce((prev,d) => {
            // If aliases exist for this data, then we have to use those.
            const dAliases = this._aliases[d.id];
            if(dAliases) {
                // If the key we are looking for is listed under one of the aliases, then it matches.
                const dAlias = dAliases.find(a => a.alias == key);
                if(dAlias) {
                    return prev.concat([{
                        data: d,
                        alias: dAlias
                    }]);
                }
            } 
            // Otherwise, refer to the data's name value.
            if(d.name == key) {
                if(dAliases) {
                    // But check if there is an undefined dAlias for this data
                    const udAlias = dAliases.find(a => typeof a.alias === 'undefined');
                    if(udAlias) {
                        return prev.concat([{
                            data: d,
                            alias: udAlias
                        }]);
                    } 
                    // If there wasn't an undefined alias available,
                    // then we shouldn't match the native name.
                } else {
                    // No aliases?
                    // Just return the data then.
                    return prev.concat([{
                        data: d
                    }]);
                }
            }
            return prev;
        }, [] as Array<{ data: Data, alias?: { alias?: string, children?: AliasMap }}>);
        
        if(matches.length == 0) return null;

        const newData = matches.map(m => m.data);
        const newAliases = matches.reduce((prev,m) => {
            if(m.alias) {
                const ret = Object.assign({}, prev);
                ret[m.data.id] = (ret[m.data.id] || []).concat([m.alias]);
                return ret;
            }
            return prev
        }, {} as AliasMap);

        return new LensData(newData, newAliases);
    }
    

    toArray() : Array<LensData> {
        const ret = [];
        for(let lData of this) {
            ret.push(lData);
        }
        return ret;
    }

    
    // Accessors

    get length() : number {
        return this._data.length;
    }

    get id() : string {
        return this.data.id;
    }

    get name() : string {
        return this.alias || this.data.name;
    }

    get type() : string {
        return this.data.type;
    }

    get value() : any {
        return this.data.data;
    }

    get children() : LensData | null {

        if(!this.data.children || this.data.children.length == 0) return null;

        // Get entry for this data in the alias map, to provide the children alias map.
        let childAliases : AliasMap = {};
        const aliases = this._aliases[this.id];
        if(aliases && aliases.length > 0) {
            // 'this' LensData's alias is the first one.
            childAliases = aliases[0].children;
        }

        return new LensData(this.data.children!, childAliases); 
    }

    get alias() : string | null {
        const aliases = this._aliases[this.id];
        // 'this' LensData's alias is the first one.
        return aliases && aliases.length && aliases[0].alias ? aliases[0].alias : null;
    }

    get data() : Data {
        return this._data[0];
    }

    get results() : Array<Data> {
        return this._data;
    }
}