import { 
    getLens as defaultGetLens,
    getLenses as defaultGetLenses,
    Data,
    AliasMap,
    ResolvedLens, 
} from '@mylens/lens-resolver';
import { decrypt, EncryptedType } from './encrypt';
import LensData from './lens-data';
import { GetLensType, GetLensesType } from './types';

export default interface Lens {
    url: string,
    privateKey: string,

    id: string,
    userId: string,
    publicKey: string,
    created: Date,
    updated: Date,
    data: Array<Data>,

    get: (key?: string) => LensData
}

export {GetLensType as getLensType}
export {GetLensesType as getLensesType}

function defaultDecryptContent(content: string, privateKey: string): string | Buffer {
    
    let cipherObject : EncryptedType;
    try {
        cipherObject = JSON.parse(content);
    } catch(err) {
        throw new Error("Failed to decrypt: target content is not a valid encrypted object");
    }

    if(!cipherObject.cipherText) throw new Error("Failed to decrypt: target content is missing the 'cipherText' property")
    if(!cipherObject.ephemeralPK) throw new Error("Failed to decrypt: target content is missing the 'ephemeralPK' property")
    if(!cipherObject.iv) throw new Error("Failed to decrypt: target content is missing the 'iv' property")
    if(!cipherObject.mac) throw new Error("Failed to decrypt: target content is missing the 'mac' property")

    return decrypt(privateKey, cipherObject);
}

export async function resolveLenses(
    requests: Array<{ url: string, privateKey: string, decryptContent?: (content: string) => string }>,
    getLenses?: GetLensesType
) : Promise<Array<{ lens?: Lens, error?: Error }>> {

    // Default getLens if necessary
    const rGetLenses = typeof getLenses === 'undefined' ? defaultGetLenses : getLenses; 

    const preppedRequests = requests.map(r => {
        // Default decryptContent if necessary
        const rDecryptContent = typeof r.decryptContent === 'undefined' 
            ? (content: string): string => {
                return defaultDecryptContent(content, r.privateKey).toString();
            }
            : r.decryptContent; 
        return {
            url: r.url,
            decrypt: rDecryptContent
        }
    });

    const remoteLenses = await rGetLenses(preppedRequests)
    const results = remoteLenses.map((r,i) => {
        if(!r) return { error: Object.assign(new Error("The target lens is unavailable. It may have been revoked!"),{ isRevoked: true }) };
        const request = requests[i];
        return { lens: new LensImpl(request.url, request.privateKey, r) };
    });

    return results;
}

export async function resolveLens(
    toResolve: { url: string, privateKey: string },
    getLens?: GetLensType,
    decryptContent?: (content : string) => string
) : Promise<Lens> {
    // Default getLens if necessary
    const rGetLens = typeof getLens === 'undefined' ? defaultGetLens : getLens; 
    // Default decryptContent if necessary
    const rDecryptContent = typeof decryptContent === 'undefined' 
        ? (content: string): string => {
            return defaultDecryptContent(content, toResolve.privateKey).toString();
        }
        : decryptContent; 

    const remoteLens = await rGetLens(toResolve.url, rDecryptContent);
    // If it fails to resolve, we should probably throw an exception.
    if(!remoteLens) throw Object.assign(new Error("The target lens is unavailable. It may have been revoked!"),{ isRevoked: true });

    return new LensImpl(toResolve.url, toResolve.privateKey, remoteLens);
}
export class LensImpl implements Lens {
    url: string;
    privateKey: string;

    private remoteLens: ResolvedLens;

    constructor(url: string, privateKey: string, remoteLens: ResolvedLens) {
        this.url = url;
        this.privateKey = privateKey;
        this.remoteLens = remoteLens;
    }

    private get isResolved() {
        return !!this.remoteLens;
    }

    get id() : string { 
        if(!this.isResolved) throw new Error("Unable to obtain 'id': Lens is not yet resolved."); 

        return this.remoteLens!.id;
    }
    get userId() : string { 
        if(!this.isResolved) throw new Error("Unable to obtain 'userId': Lens is not yet resolved."); 

        return this.remoteLens!.userId;
    }
    get publicKey() : string { 
        if(!this.isResolved) throw new Error("Unable to obtain 'publicKey': Lens is not yet resolved."); 

        return this.remoteLens!.publicKey;
    }
    get created() : Date { 
        if(!this.isResolved) throw new Error("Unable to obtain 'created': Lens is not yet resolved."); 

        return new Date(this.remoteLens!.created);
    }
    get updated() : Date { 
        if(!this.isResolved) throw new Error("Unable to obtain 'updated': Lens is not yet resolved."); 

        return new Date(this.remoteLens!.updated);
    }
    get data() : Array<Data> { 
        if(!this.isResolved) throw new Error("Unable to obtain 'data': Lens is not yet resolved."); 

        return this.remoteLens!.data;
    }    
    get aliases() : AliasMap { 
        if(!this.isResolved) throw new Error("Unable to obtain 'aliases': Lens is not yet resolved."); 

        return this.remoteLens!.aliases;
    }

    get(key?: string) : LensData {
        if(!this.isResolved) throw new Error("Unable to obtain 'get' lens data: Lens is not yet resolved."); 

        const lData = new LensData(this.data, this.aliases);
        if(key) return lData.get(key);

        return lData;
    }
}