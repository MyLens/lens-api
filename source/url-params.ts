export default class UrlParams {
    private params: { [key: string]: string | boolean };

    constructor(url: string) {
        
        const hashIndex = url.indexOf('#');
        if(hashIndex > -1)
            url = url.substring(0, hashIndex);
        const queryString = url.substring(url.indexOf('?')).replace('?',"");
        const parts = queryString.split('&');
        this.params = {};

        for(let x=0;x<parts.length;x++) {
            const part = parts[x];
            // If part is empty string, don't count it.
            if(part == "") continue;
            
            const index = part.indexOf('=');
            if(index > -1) {
                this.params[part.substring(0, index)] 
                    = part.substring(index).replace('=',"");
            } else
                this.params[part] = true;
        }
    }

    paramExists(name: string) : boolean {
        const ret = this.params[name];
        if(typeof ret !== 'undefined')
            return true;

        return false;
    }

    getParam(name: string) : string | null {
        const ret : string | boolean = this.params[name];
        if(typeof ret !== 'undefined' && ret !== true)
            return decodeURIComponent(ret as string);

        return null;
    } 

    deleteParam(name: string) {
        delete this.params[name];
    }

    toString() : string {
        if(Object.keys(this.params).length == 0) return "";
        return Object.keys(this.params).reduce((prev, p) => {
            let ret = prev;
            const val = this.params[p];
            if(prev != '?') ret += "&";
            ret += p;
            if(typeof val !== 'boolean' || !val) {
                ret += "=";
                ret += val;
            }
            return ret;
        }, "?");
    }
};