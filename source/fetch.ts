export interface FetchOptions {
    method: 'POST'|'GET',
    mode: 'cors',
    headers: {
        [header: string]: string
    },
    body?: string
}

export interface FetchResponse {
    status: number,
    json: () => Promise<any>
}

type Fetch = (url: string, options: FetchOptions) => Promise<FetchResponse>;
export default Fetch;
