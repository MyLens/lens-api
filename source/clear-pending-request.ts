import BrowserWindow from './browser-window';
import UrlParams from './url-params';

const requestParams = [
    'state', 
    'code', 
    'url', 
    'lensId', 
    'userId', 
    'userPublicKey', 
    'userPrivateKey', 
    'lensState', 
    'error', 
    'error_description', 
    'error_uri'
];

export default function clearPendingRequest(window: BrowserWindow) : void {    
    const params = new UrlParams(window.location.href);
    
    for(let p of requestParams) params.deleteParam(p);

    // Set restore the previous session. 
    const search = params.toString();

    const index = window.location.href.indexOf('?');
    if(index > -1) {
        window.history.replaceState({},"", window.location.href.substring(0, index) + search);
    }
}