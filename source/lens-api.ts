import { EventEmitter } from 'events';
import Lens, { LensImpl, resolveLens, resolveLenses } from './lens';
import BrowserWindow from './browser-window';
import Fetch from './fetch';
import UrlParams from './url-params';
import {
    LensRequest,
    LensAuthRequest, 
    LensRequestTargetType, 
    LensRequestResult, 
    LensRef,
    AuthorizeRequest,
    AuthorizeRequestResult,
    LoginRequest,
    LoginRequestResult,
    UserInfo,
    GetLensType,
    GetLensesType
} from './types';
import ScopeType from './scopeType';
import RedirectUrl from './redirect-url';
import AuthorizeValidator from './authorize-validator';
import readPendingRequest from './read-pending-request';
import clearPendingRequest from './clear-pending-request';

export interface CreateLensApiOptions {
    window?: BrowserWindow,
    fetch?: Fetch
}

export interface LensAPIOptions {
    requestUrl?: string,
    authorizationApiEndpoint?: string

    clientId: string,
    privateKey?: string,
}

const LOGIN_LOCAL_STORAGE_KEY = 'lens:authentication';

const AUTH_SESSION_STORAGE_KEY = 'lens:authentication';
interface AuthSessionStorage {
    codeChallengeVerifier: string,
    state: string,
    isUserState: boolean,
    redirectUri: string
}

export class LensAPI extends EventEmitter {
    private window: BrowserWindow;
    private fetch: Fetch;

    private requestUrl: string;
    private authorizationApiEndpoint: string;
    private clientId: string;
    private privateKey?: string;

    private logoutTimeout: NodeJS.Timeout | null = null;

    constructor(createOptions: CreateLensApiOptions, config: LensAPIOptions) {
        super();

        config = Object.assign({
            requestUrl: "https://vault.mylens.io/lens-fulfillment",
            authorizationApiEndpoint: "https://staging-authentication-service.mylens.io"
        }, config);

        this.window = createOptions.window;
        this.fetch = createOptions.fetch;

        this.requestUrl = config.requestUrl;
        this.authorizationApiEndpoint = config.authorizationApiEndpoint;
        this.clientId = config.clientId;
        this.privateKey = config.privateKey;
    }

    /* Deal with lens Request and Login request */

    beginAuthorizeRequest(request: AuthorizeRequest) {
        // Make sure the request is valid, normalize it and provide us with a copy.
        const validator = new AuthorizeValidator()
        const isUserState = typeof request.state !== 'undefined';
        request = validator.normalize(request);

        validator.validate(request);

        // Generate the URL
        const { url, codeChallengeVerifier } = new RedirectUrl(this.requestUrl, this.clientId).build(request);
        
        // Keep a few things in the session
        const loginSession : AuthSessionStorage = {
            codeChallengeVerifier: codeChallengeVerifier,
            state: request.state,
            isUserState: isUserState,
            redirectUri: request.redirectUri
        };
        this.window.sessionStorage.setItem(AUTH_SESSION_STORAGE_KEY, JSON.stringify(loginSession));

        // Redirect the user.
        this.window.location.href = url;
    }

    async endAuthorizeRequest() : Promise<AuthorizeRequestResult|undefined> {
        const pendingRequest = readPendingRequest(this.window);

        if(pendingRequest) {

            // Before continuing, we need to make sure they're actually set up
            // to complete a request.
            const sAuthSession = this.window.sessionStorage.getItem(AUTH_SESSION_STORAGE_KEY);

            if(sAuthSession) {

                // At this point we can assume this is supposed to be a login scenario
                // and we can safely consume the session variable.
                this.window.sessionStorage.removeItem(AUTH_SESSION_STORAGE_KEY);

                // Also remove our querystring properties.
                clearPendingRequest(this.window);


                // Must be a JSON object.
                let authSession : AuthSessionStorage;
                try {
                    authSession = JSON.parse(sAuthSession);
                } catch(err) {
                    throw new Error(`Session variable '${AUTH_SESSION_STORAGE_KEY}' invalid: unable to parse as JSON`);
                }
                            
                // Needs to have all the properties.
                if(!authSession.codeChallengeVerifier) throw new Error(`Session variable '${AUTH_SESSION_STORAGE_KEY}' invalid: missing 'codeChallengeVerifier'`);
                if(typeof authSession.state === 'undefined') throw new Error(`Session variable '${AUTH_SESSION_STORAGE_KEY}' invalid: missing 'state'`);
                if(!authSession.redirectUri) throw new Error(`Session variable '${AUTH_SESSION_STORAGE_KEY}' invalid: missing 'redirectUri'`);
                if(typeof authSession.isUserState === 'undefined') throw new Error(`Session variable '${AUTH_SESSION_STORAGE_KEY}' invalid: missing 'isUserState'`);

                // State must match
                if(pendingRequest.state !== authSession.state) throw new Error(`Querystring parameter 'state' does not match session variable 'state'`);
                
                // If we received an error, throw that
                if(pendingRequest.error) {

                    // Throw 'error' type error.
                    const error = new Error(`Failed to obtain authorize request due to error: ${pendingRequest.error.error}`);
                    const fatError : {
                        code: string,
                        description?: string,
                        uri?: string,
                        state?: string
                    } = Object.assign({}, error, {
                        code: pendingRequest.error.error,
                    });
                    if(pendingRequest.error.description) fatError.description = pendingRequest.error.description;
                    if(pendingRequest.error.uri) fatError.uri = pendingRequest.error.uri;
                    if(authSession.isUserState) fatError.state = pendingRequest.state;

                    throw fatError;
                }

                // Everything checks out, so we need to exchange our code.
                // Submit code and receive token.
                const result = await this.fetch(this.authorizationApiEndpoint + '/tokens', {
                    method: 'POST',
                    mode:'cors',  
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: 
                        JSON.stringify({
                            grant_type: 'authorization_code', 
                            code: pendingRequest.result.code, 
                            client_id: this.clientId,
                            redirect_uri: authSession.redirectUri,
                            code_verifier: authSession.codeChallengeVerifier
                        })
                    
                })

                if (result.status !== 200){
                    // problem
                    try {
                        let message = await result.json(); 
                        throw new Error(`Error code: ${message.error}, Error Message: ${message.error_message}`)
                    } catch(e) {
                        throw new Error(`Received status code ${result.status} and could not decode the resulting message as json.`)
                    }
                }

                const resultJson = await result.json(); 

                // Need to translate a few props before storing.
            
                // Explicit future expiration date
                const expires = Date.now() + resultJson.expires_in * 1000;
                
                const authResult : AuthorizeRequestResult = {
                    accessToken: resultJson.access_token,
                    idToken: resultJson.id_token,
                    expires: new Date(expires),
                    scope: !resultJson.scope || resultJson.scope === '' ? [] : resultJson.scope.split(' '),
                };
                // Only include the state if the user defined it.
                if(authSession.isUserState) authResult.state = pendingRequest.state;

                // Include the lens request.
                if(pendingRequest.result.lensResult) authResult.lensRequest = pendingRequest.result.lensResult;


                // Save the resulting login to local storage.
                this.saveLoginResult(authResult as LoginRequestResult);

                // We have a full auth result!
                return authResult;
            }
        }

        return;
    }

    isPendingAuthorizeRequest() : boolean {
        // The right query needs to exist.
        const pendingRequest = readPendingRequest(this.window);
    
        // Key needs to be in session storage.
        const sAuthSession = this.window.sessionStorage.getItem(AUTH_SESSION_STORAGE_KEY);
    
        return !!pendingRequest && !!sAuthSession;
    }

    /* Deal with Login Request */

    beginLoginRequest(request: LoginRequest) {
        this.beginAuthorizeRequest(request);
    }

    async endLoginRequest() : Promise<LoginRequestResult|undefined> {
        return this.endAuthorizeRequest();
    }


    private saveLoginResult(login: LoginRequestResult) {

        // Do a little serialization here.
        const toSave : {
            accessToken: string,
            idToken: string,
            expires: number,
            scope: Array<ScopeType>,
            state?: string
        } = {
            accessToken: login.accessToken,
            idToken: login.idToken,
            expires: login.expires.getTime(),
            scope: login.scope,
        }
        if(login.state) toSave.state = login.state;

        this.window.localStorage.setItem(LOGIN_LOCAL_STORAGE_KEY, JSON.stringify(toSave)); 

        // At this point, we explicitly know their login on this object, so we can set the logout
        
        // Set up a timeout to log them out upon expiration, offset 5 minutes
        this.setupAutoLogout(toSave.expires);
    }

    private loadLoginResult() : LoginRequestResult {
        const sLogin = this.window.localStorage.getItem(LOGIN_LOCAL_STORAGE_KEY);
        // Nothing there? Send nothing back.
        if(!sLogin) {
            // Special failure.
            const err = new Error("Session does not exist");
            throw Object.assign(err, {
                loggedOut: true
            });
        }

        const login = JSON.parse(sLogin);

        // Ensure all the right stuff exists.
        if(!login.accessToken || typeof login.accessToken !== 'string') throw new Error(`Saved session variable '${LOGIN_LOCAL_STORAGE_KEY}' invalid: missing or invalid 'accessToken'`);
        if(!login.idToken || typeof login.idToken !== 'string') throw new Error(`Saved session variable '${LOGIN_LOCAL_STORAGE_KEY}' invalid: missing or invalid 'idToken'`);
        if(!login.expires || typeof login.expires !== 'number') throw new Error(`Saved session variable '${LOGIN_LOCAL_STORAGE_KEY}' invalid: missing or invalid 'expires'`);
        if(!login.scope || typeof login.scope !== 'object' || !(login.scope instanceof Array)) throw new Error(`Saved session variable '${LOGIN_LOCAL_STORAGE_KEY}' invalid: missing or invalid 'scope'`);
        if(login.state && typeof login.state !== 'string') throw new Error(`Saved session variable '${LOGIN_LOCAL_STORAGE_KEY}' invalid: invalid 'state'`);

        const toLoad : LoginRequestResult = {
            accessToken: login.accessToken,
            idToken: login.idToken,
            expires: new Date(login.expires),
            scope: login.scope,
        };
        if(login.state) toLoad.state;

        // At this point, we explicitly know their login on this object, so we can set the logout
        
        // Set up a timeout to log them out upon expiration
        this.setupAutoLogout(login.expires);


        return toLoad;
    }

    private setupAutoLogout(expireTime) {
        if(this.logoutTimeout) clearTimeout(this.logoutTimeout);

        // offset 5 minutes
        const autoExpireTime = expireTime - (5 * 60 * 1000); 

        this.logoutTimeout = setTimeout(() => { 
                // Check if we actually are expiring before logging out.

                if(Date.now() < autoExpireTime) {
                    // Then we're not ready to expire.
                    // Set up the autologout again.
                    this.setupAutoLogout(expireTime);
                } else {
                    // Log us out.
                    this.logout(); 
                }
            }, 
            // Max value for timer is ~24 days. We'll play it safe and only let it go for 7.
            Math.min(7*24*60*60*1000, Math.max(0, (expireTime - Date.now()) - (5 * 60 * 1000)))
        );
    }
    
    logout() {
        // We are only logged in if our logout timer exists.
        if(this.logoutTimeout) { 
            clearTimeout(this.logoutTimeout); 
            this.logoutTimeout = null;
            this.window.localStorage.removeItem(LOGIN_LOCAL_STORAGE_KEY);
            this.emit('logout');
        }
    }

    isPendingLoginRequest() : boolean {
        return this.isPendingAuthorizeRequest();
    }

    getLoginRequest() : LoginRequestResult | undefined {
        try {
            const login = this.loadLoginResult();
            return login;
        } catch(err) {
            if(!err.loggedOut) {
                console.log("Failed to load session:", err);
                // If we fail, treat it as a log out.
                this.logout();
            }
        }
        // If we failed, nothing gets sent back.
        return;
    }

    /* Deal with lens Requests */

    beginLensRequest(request: LensRequest) : void {
        const existingLoginRequest = this.getLoginRequest();
        const authRequest: AuthorizeRequest = {
            redirectUri: request.redirectUri,
            scope: existingLoginRequest ? existingLoginRequest.scope : [ScopeType.OpenId],
            lensRequest: request,
            vLoginScreen:request.vLoginScreen,
        };
        if(existingLoginRequest && existingLoginRequest.state) authRequest.state = existingLoginRequest.state;

        this.beginAuthorizeRequest(authRequest);
    }

    async endLensRequest() : Promise<LensRequestResult|undefined> {
        const authResult = await this.endAuthorizeRequest();
        if(!authResult) return;

        return authResult.lensRequest;
    }

    async resolveLens(ref: LensRef, getLens?: GetLensType, decryptContent?: (content: string) => string) : Promise<Lens> {
        const privateKey = ref.privateKey || this.privateKey;
        if(!privateKey) throw new Error(`The privateKey is missing from LensRef: ${ref.url}.`);
        const lens = await resolveLens({ url: ref.url, privateKey }, getLens, decryptContent);
        return lens;
    }

    async resolveLenses(refs: Array<LensRef|{ lensRef: LensRef, decrypt?: (content: string) => string}>, getLenses?: GetLensesType) : Promise<Array<{ lens?: Lens, error?: Error }>> {
        // If a ref doesn't have a private key but one is provided for this api instance, apply that.
        const toResolve = refs.map(ref => {
            const lrd = ref as { lensRef: LensRef, decrypt?: (content: string) => string};
            if(lrd.lensRef) {
                // Make sure a key of some sort exists.
                if(!lrd.lensRef.privateKey && !this.privateKey) throw new Error(`The privateKey is missing from LensRef: ${lrd.lensRef.url}.`);
                return {
                    url: lrd.lensRef.url,
                    privateKey: lrd.lensRef.privateKey || this.privateKey,
                    decrypt: lrd.decrypt
                };
            }
            const lr = ref as LensRef;
            // Make sure a key of some sort exists.
            if(!lr.privateKey && !this.privateKey) throw new Error(`The privateKey is missing from LensRef: ${lr.url}.`);
            return {
                url: lr.url,
                privateKey: lr.privateKey || this.privateKey,
            };
        });

        const lenses = await resolveLenses(toResolve, getLenses);

        return lenses;
    }


    async getUserInfo() : Promise<UserInfo> {
        // Make sure we have the access token
        let loginResult = this.loadLoginResult(); 
       
        const result = await this.fetch(this.authorizationApiEndpoint + '/userinfo', {
            method: 'GET',
            mode:'cors',  
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + loginResult.accessToken
            },
            
        })

        if (result.status !== 200){
            // problem
            try {
                let message = await result.json(); 
                throw new Error(`Error code: ${message.error}, Error Message: ${message.error_message}`)
            } catch(e) {
                throw new Error(`Received status code ${result.status} and could not fetch user info.`)
            }
        }

        const resultJson = await result.json(); 
        if (!resultJson.sub) throw new Error(`The userinfo returned from the authorization endpoint does not contain the "sub" field.`); 

        return {
            userId: resultJson.sub, 
            preferredUsername: resultJson.preferred_username
        }

    }
}

function createLensAPI(options: CreateLensApiOptions) : {
    new(config: LensAPIOptions): LensAPI
} {
    return LensAPI.bind(LensAPI, options);
};

export default createLensAPI;
