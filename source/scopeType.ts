
enum ScopeType {
    OpenId='openid', // Request basic OIDC token
    Profile='profile', // Request basic profile info in idToken/userinfo (preferred_username)
    Identity='identity', // Request access to manage identities for this user.
    Vault='vault', // Request access to manage the vault for this user.
    Enterprise='enterprise', // Full access to try and manage enterprise-service project resources
    Enterprise_LensRefs='enterprise:lensrefs' // Access to manage lensRefs for this user on the enterprise-service
}

export const ScopeTypes: Array<ScopeType> = Object.keys(ScopeType).map(e => ScopeType[e as any] as ScopeType);

export function limitScopes(...scopeGroups: Array<Array<ScopeType>>) : Array<ScopeType> {
    if(scopeGroups.length == 0) return [];

    return scopeGroups.reduce((prev,g) => prev.filter(s => g.includes(s)), scopeGroups[0]);
}

export function unionScopes(...scopeGroups: Array<null|Array<ScopeType>>) : Array<ScopeType> {
    if(scopeGroups.length == 0) return [];

    const scopeSet = new Set<ScopeType>(
        scopeGroups.reduce((prev,g) => g ? prev.concat(g) : prev, [])
    );
    return [...scopeSet];
}

export function stringifyScopes(scopes: Array<ScopeType>) : string {
    return scopes.join(' ');
}

export function parseScopes(scope: string) : Array<ScopeType> {
    const aScopes : Array<string> = scope.split(' ');
    // Farm out invalid ones.
    return aScopes.filter(s => ScopeTypes.includes(s as ScopeType)) as Array<ScopeType>;
}

export const DefaultScopes : Array<ScopeType> = [ScopeType.OpenId];

export default ScopeType;
