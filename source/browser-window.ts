export interface Storage {
    getItem: (key: string) => string,
    removeItem: (key: string) => void,
    setItem: (key: string, value: string) => void
}
export default interface BrowserWindow {
    location: {
        // Only including properties and functions we need
        href: string,
        search: string,
    },
    history: {
        replaceState: (stateObj: any, title: string, url?: string) => void
    },
    sessionStorage: Storage,
    localStorage: Storage
}