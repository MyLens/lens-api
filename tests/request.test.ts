import createLensAPI from '../source/lens-api';
import { LensRequestTargetType } from '../source/types';

test('A default lens request is sent', async () => {

    const clientId = 'vendor001';
    const state = 'state';
    const redirectUri = "https://fake.testing.com";
    const requestUrl = "https://fake.lens.io/request";
    const data = [{
            name: 'firstName',
            type: 'string',
            value: 'Nathan'
        },
        {
            name: 'lastName'
        }
    ];

    let _href = redirectUri;
    let _search = "";

    const LensAPI = createLensAPI({
        window: {
            location: {
                get href() { 
                    return _href;
                },
                set href(value) {
                    _href = value;
                },
                get search() {
                    return _search;
                },
                set search(value) {
                    _search = value;
                    this.href = this.href.substring(0,this.href.indexOf('?')) + _search;
                }
            },
            history: {
                replaceState() {}
            },
            sessionStorage: { getItem(key) { return ""; }, removeItem(key){}, setItem(key, value){} },
            localStorage: { getItem(key){ return ""; }, removeItem(key){}, setItem(key, value){}}    
        },
        fetch: () => {
            return new Promise((resolve) => {
                resolve({
                    status: 200,
                    json: () => {
                        return new Promise((resolve) => {
                            resolve({});
                        })
                    }
                });
            });
        }
    });

    const api = new LensAPI({
        clientId: clientId,
        requestUrl: requestUrl
    });

    api.beginAuthorizeRequest({
        redirectUri: redirectUri,
        state: state,
        lensRequest: {
            data: data
        }
    });

    const href = (requestUrl
        + `?response_type=code`
        + `&scope=openid`
        + `&client_id=${encodeURIComponent(clientId)}`
        + `&redirect_uri=${encodeURIComponent(redirectUri)}`
        + `&state=${encodeURIComponent(state)}`
        + `&code_challenge=`
        ).replace(/[.*+?^${}()|[\]\\]/g, '\\$&')
        + ".+?"
        +(`&code_challenge_method=S256`
        + `&data=${encodeURIComponent(Buffer.from(JSON.stringify(data)).toString('base64'))}`
        ).replace(/[.*+?^${}()|[\]\\]/g, '\\$&');

    expect(_href).toMatch(new RegExp(href));
});

test('A self lens request is sent', async () => {

    const clientId = 'vendor001';
    const state = 'state';
    const redirectUri = "https://fake.testing.com";
    const requestUrl = "https://fake.lens.io/request";
    const data = [{
            name: 'firstName',
            type: 'string',
            value: 'Nathan'
        },
        {
            name: 'lastName'
        }
    ];

    let _href = redirectUri;
    let _search = "";

    const LensAPI = createLensAPI({
        window: {
            location: {
                get href() { 
                    return _href;
                },
                set href(value) {
                    _href = value;
                },
                get search() {
                    return _search;
                },
                set search(value) {
                    _search = value;
                    this.href = this.href.substring(0,this.href.indexOf('?')) + _search;
                }
            },
            history: {
                replaceState() {}
            },
            sessionStorage: { getItem(key) { return ""; }, removeItem(key){}, setItem(key, value){} },
            localStorage: { getItem(key){ return ""; }, removeItem(key){}, setItem(key, value){}}    
        },
        fetch: () => {
            return new Promise((resolve) => {
                resolve({
                    status: 200,
                    json: () => {
                        return new Promise((resolve) => {
                            resolve({});
                        })
                    }
                });
            });
        }
    });

    const api = new LensAPI({
        clientId: clientId,
        requestUrl: requestUrl
    });

    api.beginAuthorizeRequest({
        redirectUri: redirectUri,
        state: state,
        lensRequest: {
            data: data,
            target: LensRequestTargetType.Self
        }
    });

    const href = (requestUrl
        + `?response_type=code`
        + `&scope=openid`
        + `&client_id=${encodeURIComponent(clientId)}`
        + `&redirect_uri=${encodeURIComponent(redirectUri)}`
        + `&state=${encodeURIComponent(state)}`
        + `&code_challenge=`
        ).replace(/[.*+?^${}()|[\]\\]/g, '\\$&')
        + ".+?"
        +(`&code_challenge_method=S256`
        + `&targetType=${encodeURIComponent(LensRequestTargetType.Self)}`
        + `&data=${encodeURIComponent(Buffer.from(JSON.stringify(data)).toString('base64'))}`
        ).replace(/[.*+?^${}()|[\]\\]/g, '\\$&');

    expect(_href).toMatch(new RegExp(href));
});

test('A general (create new user) lens request is sent', async () => {

    const clientId = 'vendor001';
    const state = 'state';
    const redirectUri = "https://fake.testing.com";
    const requestUrl = "https://fake.lens.io/request";
    const data = [{
            name: 'firstName',
            type: 'string',
            value: 'Nathan'
        },
        {
            name: 'lastName'
        }
    ];

    let _href = redirectUri;
    let _search = "";

    const LensAPI = createLensAPI({
        window: {
            location: {
                get href() { 
                    return _href;
                },
                set href(value) {
                    _href = value;
                },
                get search() {
                    return _search;
                },
                set search(value) {
                    _search = value;
                    this.href = this.href.substring(0,this.href.indexOf('?')) + _search;
                }
            },
            history: {
                replaceState() {}
            },
            sessionStorage: { getItem(key) { return ""; }, removeItem(key){}, setItem(key, value){} },
            localStorage: { getItem(key){ return ""; }, removeItem(key){}, setItem(key, value){}}    
        },
        fetch: () => {
            return new Promise((resolve) => {
                resolve({
                    status: 200,
                    json: () => {
                        return new Promise((resolve) => {
                            resolve({});
                        })
                    }
                });
            });
        }
    });

    const api = new LensAPI({
        clientId: clientId,
        requestUrl: requestUrl
    });

    api.beginAuthorizeRequest({
        redirectUri: redirectUri, 
        state: state,
        lensRequest: {
            data: data,
            target: LensRequestTargetType.General
        }
    });

    const href = (requestUrl
        + `?response_type=code`
        + `&scope=openid`
        + `&client_id=${encodeURIComponent(clientId)}`
        + `&redirect_uri=${encodeURIComponent(redirectUri)}`
        + `&state=${encodeURIComponent(state)}`
        + `&code_challenge=`
        ).replace(/[.*+?^${}()|[\]\\]/g, '\\$&')
        + ".+?"
        +(`&code_challenge_method=S256`
        + `&targetType=${encodeURIComponent(LensRequestTargetType.General)}`
        + `&data=${encodeURIComponent(Buffer.from(JSON.stringify(data)).toString('base64'))}`
        ).replace(/[.*+?^${}()|[\]\\]/g, '\\$&');

    expect(_href).toMatch(new RegExp(href));
});

test('A general (create for existing user) lens request is sent', async () => {

    const clientId = 'vendor001';
    const state = 'state';
    const redirectUri = "https://fake.testing.com";
    const requestUrl = "https://fake.lens.io/request";
    const data = [{
            name: 'firstName',
            type: 'string',
            value: 'Nathan'
        },
        {
            name: 'lastName'
        }
    ];

    const userId = "121212testguy";
    const userPublicKey = "PKxy129s91";


    let _href = redirectUri;
    let _search = "";

    const LensAPI = createLensAPI({
        window: {
            location: {
                get href() { 
                    return _href;
                },
                set href(value) {
                    _href = value;
                },
                get search() {
                    return _search;
                },
                set search(value) {
                    _search = value;
                    this.href = this.href.substring(0,this.href.indexOf('?')) + _search;
                }
            },
            history: {
                replaceState() {}
            },
            sessionStorage: { getItem(key) { return ""; }, removeItem(key){}, setItem(key, value){} },
            localStorage: { getItem(key){ return ""; }, removeItem(key){}, setItem(key, value){}}    
        },
        fetch: () => {
            return new Promise((resolve) => {
                resolve({
                    status: 200,
                    json: () => {
                        return new Promise((resolve) => {
                            resolve({});
                        })
                    }
                });
            });
        }
    });

    const api = new LensAPI({
        clientId: clientId,
        requestUrl: requestUrl
    });

    api.beginAuthorizeRequest({
        redirectUri: redirectUri, 
        state: state,
        lensRequest: {
            data: data,
            target: {
                type: LensRequestTargetType.General,
                id: userId,
                publicKey: userPublicKey
            }
        }
    });


    const href = (requestUrl
        + `?response_type=code`
        + `&scope=openid`
        + `&client_id=${encodeURIComponent(clientId)}`
        + `&redirect_uri=${encodeURIComponent(redirectUri)}`
        + `&state=${encodeURIComponent(state)}`
        + `&code_challenge=`
        ).replace(/[.*+?^${}()|[\]\\]/g, '\\$&')
        + ".+?"
        +(`&code_challenge_method=S256`
        + `&targetType=${encodeURIComponent(LensRequestTargetType.General)}`
        + `&targetId=${encodeURIComponent(userId)}`
        + `&targetPublicKey=${encodeURIComponent(userPublicKey)}`
        + `&data=${encodeURIComponent(Buffer.from(JSON.stringify(data)).toString('base64'))}`
        ).replace(/[.*+?^${}()|[\]\\]/g, '\\$&');

    expect(_href).toMatch(new RegExp(href));
});