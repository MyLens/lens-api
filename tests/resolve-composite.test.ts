test("Empty", () =>{});

// import LensStorage, {
//     MockUserSession, 
//     hashSign, 
//     pubKeyFromPrivKeyDummy, 
//     MOCK_DOMAIN,
//     AliasMap
// } from '@mylens/lens-storage'; 
// import { createGetLens } from '@mylens/lens-resolver';
// import FetchAdapter from '@mylens/lens-resolver/dist/source/fetch-adapter';

// import { LensAPI } from '../source/lens-api'; 
// import { FetchOptions, FetchResponse } from '../source/fetch';
// import LensData from '../source/lens-data';

// class MockFetchAdapter extends MockUserSession implements FetchAdapter {
//     async fetch(url: string) : Promise<any> {
//         const results: (null | string) = await this.getFile(url.replace(MOCK_DOMAIN, ""));
//         if(results == null) return null;
//         return JSON.parse(results);
//     }
// }


// test('Resolve a composite lens', async () => {
//     let session = new MockFetchAdapter(); 
//     let storage = new LensStorage(session, hashSign, pubKeyFromPrivKeyDummy); 
//     // Create two pieces of data. 
//     let d1 = await storage.createData({ name: 'phoneNumber', type: 'string/phone-number', data: '+15051234567' }); 
//     let d2 = await storage.createData({ name: 'firstName', type: 'string', data: 'Homer' }); 

//     // Create composite data type
//     const personComposite = await storage.createData({ name: 'person', type: 'composite', children: [d1.id, d2.id]}); 
//     // Create additional data to add to the composite. 
//     const d3 = await storage.createData({ name: 'emailAddress', type: 'string/email-address', data: 'homer@mylens.io' });

//     // Create a lens that contains the composite type and the email address WITH a name map. 
//     let aliases : AliasMap = {
//         [personComposite.id]:[
//             { 
//                 alias: 'emergencyContact',
//                 children: {
//                     // Rename first name to given name and name. 
//                     [d2.id]: [{ alias: 'givenName'}, { alias: 'name'}]
//                     // Don't rename phoneNumber. 
//                 }
//             }, 
//             { 
//                 alias: 'eContact',
//                 children: {
//                     // Rename first name to given name and name. 
//                     [d2.id]: [{ alias: 'givenName'}, { alias: 'name'}]
//                     // Don't rename phoneNumber. 
//                 }
//             }
//         ], // Map person composite to emergecy contact
//         [d3.id]: [ { alias: 'eAddress' }] 
//     };

//     const eContactLens = await storage.createLens({label: "Emergency Contact"}, 'lensId', 
//         [d3.id,personComposite.id], {lat: 0, long: 0}, aliases); 
    
//     // Create Lens api with mocked goodness 
//     let fetch = async (url: string, options: FetchOptions) : Promise<FetchResponse> => {
//         let res = await session.fetch(url);   
//         return {
//             status: (res !== null) ? 200 : 400, 
//             json: () => res
//         }  
//     }

//     let api = new LensAPI({fetch: fetch}, {
//                             clientId: 'SomeId', 
//                             authorizationApiEndpoint: 'do not care', 
//                             privateKey: "blahblah", 
//                             requestUrl: "who cares"})


//     const decryptContent = (content: string) : string => {
//         return session.decryptContent(content); 
//     }

//     const getLens = createGetLens(session); 

//     const result = await api.resolveLens(eContactLens, getLens, decryptContent); 
//     let contact = result.get('emergencyContact'); 

//     expect(contact).not.toBe(null);
//     expect(contact.type).toBe('composite'); 
//     let contactChildren = contact.children; 
//     expect(contactChildren.get('givenName').value).toBe('Homer'); 

//     let eContact = result.get('eContact'); 
//     expect(eContact.type).toBe('composite'); 
//     expect(eContact).not.toBe(null); 

//     let email = result.get('eAddress'); 
//     expect(email.type).toBe('string/email-address'); 
//     expect(email.value).toBe('homer@mylens.io'); 

// })
