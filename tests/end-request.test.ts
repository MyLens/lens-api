import createLensAPI from '../source/lens-api';
import { LensRequestResult } from '../source/types';

test('A lens request for a new user is returned and ended', async () => {

    const clientId = 'not-real-business';
    const userId = 'not-real-user';
    const publicKey = 'abc123';
    const privateKey = "2a0dffeb4fe2b2cc5c6e4379fda3913fcf525d4b709a755c5086ad0ae6cf00f7"
    const redirectUrl = "https://fake.testing.com";
    const requestUrl = "https://fake.lens.io/request";
    const lensUrl = "https://gaia.blockstack.org/hub/15Bg8gbLKYfmJiEsRjdnmo5rUXQLF1Th39/non-lens-user-shares/9ebe048b-cb51-44d5-97f4-f71203de7910.json"
    const lensId = "9ebe048b-cb51-44d5-97f4-f71203de7910";
    const code = "somecode";

    const expiresIn = 6 * 60;
    const accessToken = "accesstoken";
    const idToken = "idtoken";
    const sScope = "openid";
    const state = "state";

    let _href = redirectUrl
        + `?url=${encodeURIComponent(lensUrl)}`
        + `&code=${encodeURIComponent(code)}`
        + `&state=${encodeURIComponent(state)}`
        + `&lensId=${encodeURIComponent(lensId)}`
        + `&userId=${encodeURIComponent(userId)}`
        + `&userPublicKey=${encodeURIComponent(publicKey)}`
        + `&userPrivateKey=${encodeURIComponent(privateKey)}`
        ;
    let _search = "";

    const LensAPI = createLensAPI({
        window: {
            location: {
                get href() { 
                    return _href;
                },
                set href(value) {
                    _href = value;
                },
                get search() {
                    return _search;
                },
                set search(value) {
                    _search = value;
                    this.href = this.href.substring(0,this.href.indexOf('?')) + _search;
                }
            },
            history: {
                replaceState() {}
            },
            sessionStorage: {
                getItem(key) {
                    return JSON.stringify({
                        codeChallengeVerifier: "whatever",
                        state: state,
                        isUserState: false,
                        redirectUri: redirectUrl
                    })
                }, removeItem(key){}, setItem(key, value){}
            },
            localStorage: { getItem(key){ return ""; }, removeItem(key){}, setItem(key, value){}}
        },
        fetch: () => {
            return new Promise((resolve) => {
                resolve({
                    status: 200,
                    json: () => {
                        return new Promise((resolve) => {
                            resolve({
                                expires_in: expiresIn,                                
                                access_token: accessToken,
                                id_token: idToken,
                                scope: sScope
                            });
                        })
                    }
                });
            });
        }
    });

    const api = new LensAPI({
        clientId: clientId,
        requestUrl: requestUrl
    });

    const result : LensRequestResult = await api.endLensRequest();

    expect(result.lens.url).toBe(lensUrl);
    expect(result.lens.privateKey).toBe(privateKey);
    expect(result.user.id).toBe(userId);
    expect(result.user.publicKey).toBe(publicKey);
    expect(result.user.privateKey).toBe(privateKey);
    expect(result).not.toHaveProperty('state');
});

test('A lens request for self is returned and ended', async () => {

    const clientId = 'not-real-business';
    const redirectUrl = "https://fake.testing.com";
    const requestUrl = "https://fake.lens.io/request";
    const lensUrl = "https://gaia.blockstack.org/hub/15Bg8gbLKYfmJiEsRjdnmo5rUXQLF1Th39/non-lens-user-shares/9ebe048b-cb51-44d5-97f4-f71203de7910.json"
    const lensId = "9ebe048b-cb51-44d5-97f4-f71203de7910";
    const lensState = "some custom vendor state";
    const code = "somecode";

    const expiresIn = 6 * 60;
    const accessToken = "accesstoken";
    const idToken = "idtoken";
    const sScope = "openid";
    const state = "state";

    let _href = redirectUrl
        + `?url=${encodeURIComponent(lensUrl)}`
        + `&code=${encodeURIComponent(code)}`
        + `&state=${encodeURIComponent(state)}`
        + `&lensId=${encodeURIComponent(lensId)}`
        + `&lensState=${encodeURIComponent(lensState)}`
        ;
    let _search = "";

    const LensAPI = createLensAPI({
        window: {
            location: {
                get href() { 
                    return _href;
                },
                set href(value) {
                    _href = value;
                },
                get search() {
                    return _search;
                },
                set search(value) {
                    _search = value;
                    this.href = this.href.substring(0,this.href.indexOf('?')) + _search;
                }
            },
            history: {
                replaceState() {}
            },
            sessionStorage: {
                getItem(key) {
                    return JSON.stringify({
                        codeChallengeVerifier: "whatever",
                        state: state,
                        isUserState: false,
                        redirectUri: redirectUrl
                    })
                }, removeItem(key){}, setItem(key, value){}
            },
            localStorage: { getItem(key){ return ""; }, removeItem(key){}, setItem(key, value){}}    
        },
        fetch: () => {
            return new Promise((resolve) => {
                resolve({
                    status: 200,
                    json: () => {
                        return new Promise((resolve) => {
                            resolve({
                                expires_in: expiresIn,                                
                                access_token: accessToken,
                                id_token: idToken,
                                scope: sScope
                            });
                        })
                    }
                });
            });
        }
    });

    const api = new LensAPI({
        clientId: clientId,
        requestUrl: requestUrl
    });

    const result : LensRequestResult = await api.endLensRequest();

    expect(result.lens.url).toBe(lensUrl);
    expect(result.state).toBe(lensState);
    expect(result.lens).not.toHaveProperty('privateKey');
    expect(result).not.toHaveProperty('user');
});
