test("Empty", () =>{});

// import LensStorage, {
//     MockUserSession, 
//     hashSign, 
//     pubKeyFromPrivKeyDummy, 
//     MOCK_DOMAIN,
//     AliasMap
// } from '@mylens/lens-storage'; 
// import { createGetLens, createGetLenses } from '@mylens/lens-resolver';
// import FetchAdapter from '@mylens/lens-resolver/dist/source/fetch-adapter';

// import { LensAPI } from '../source/lens-api'; 
// import { FetchOptions, FetchResponse } from '../source/fetch';
// import LensData from '../source/lens-data';

// class MockFetchAdapter extends MockUserSession implements FetchAdapter {
//     async fetch(url: string) : Promise<any> {
//         const results: (null | string) = await this.getFile(url.replace(MOCK_DOMAIN, ""));
//         if(results == null) return null;
//         return JSON.parse(results);
//     }
// }


// test('Resolve multiple lenses at once.', async () => {
//     let session = new MockFetchAdapter(); 
//     let storage = new LensStorage(session, hashSign, pubKeyFromPrivKeyDummy); 
//     // Create three pieces of data. 
//     let d1 = await storage.createData({ name: 'phoneNumber', type: 'string/phone-number', data: '+15051234567' }); 
//     let d2 = await storage.createData({ name: 'firstName', type: 'string', data: 'Homer' }); 
//     let d3 = await storage.createData({ name: 'lastName', type: 'string', data: 'Simpson' }); 
//     let aliases : AliasMap = {};

//     const lens1 = await storage.createLens({label: "Lens 1"}, 'lensId', [d1.id,d2.id], {lat: 0, long: 0}, aliases); 
//     const lens2 = await storage.createLens({label: "Lens 2"}, 'lensId', [d2.id,d3.id], {lat: 0, long: 0}, aliases); 
    
//     // Create Lens api with mocked goodness 
//     let fetch = async (url: string, options: FetchOptions) : Promise<FetchResponse> => {
//         let res = await session.fetch(url);   
//         return {
//             status: (res !== null) ? 200 : 400, 
//             json: () => res
//         }  
//     }

//     let api = new LensAPI({fetch: fetch}, {
//                             clientId: 'SomeId', 
//                             authorizationApiEndpoint: 'do not care', 
//                             privateKey: "blahblah", 
//                             requestUrl: "who cares"})


//     const decryptContent = (content: string) : string => {
//         return session.decryptContent(content); 
//     }

//     const getLenses = createGetLenses(session); 

//     session.resetNumCalls();

//     const results = await api.resolveLenses([
//         { lensRef: lens1, decrypt: decryptContent }, 
//         { lensRef: lens2, decrypt: decryptContent }
//     ], getLenses); 
//     const rLens1 = results[0].lens.get(); 
//     const rLens2 = results[1].lens.get(); 

//     expect(rLens1).not.toBe(null);
//     expect(rLens1.get('firstName').value).toBe('Homer'); 
//     expect(rLens1.get('phoneNumber').value).toBe('+15051234567'); 
    
//     expect(rLens2).not.toBe(null);
//     expect(rLens2.get('firstName').value).toBe('Homer'); 
//     expect(rLens2.get('lastName').value).toBe('simpson');

//     const totalReads = session.getNumGetFileCalls();
//     // Expect 1 read per lens and 1 data file read.
//     expect(totalReads).toBe(3);
// })
