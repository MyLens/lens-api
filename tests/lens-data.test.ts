import { Data, AliasMap } from '@mylens/lens-resolver';
import LensData from '../source/lens-data';

function generateData() : Array<Data> {
    return [
        {
            id: 'idxfirstname123',
            name: 'firstName',
            type: 'Text',
            publicKey: 'abc123',
            signature: 'xyz',
            data: 'Bob',
            verifiedBy: []
        },
        {
            id: 'idxlastname123',
            name: 'lastName',
            type: 'Text',
            publicKey: 'abc123',
            signature: 'xyz',
            data: 'Smith',
            verifiedBy: []
        },
        {
            id: 'idxemailAddress123',
            name: 'emailAddress',
            type: 'Text',
            publicKey: 'abc123',
            signature: 'xyz',
            data: 'bob@gmail.com',
            verifiedBy: []
        },
        {
            id: 'idxemailAddress2123',
            name: 'emailAddress',
            type: 'Text',
            publicKey: 'abc123',
            signature: 'xyz',
            data: 'bob.smith@companyA.com',
            verifiedBy: []
        },
        {
            id: 'idxphoneNumber123',
            name: 'phoneNumber',
            type: 'Text',
            publicKey: 'abc123',
            signature: 'xyz',
            data: '+15501234567',
            verifiedBy: []
        },
        {
            id: 'idxphoneNumberWork123',
            name: 'phoneNumberWork',
            type: 'Text',
            publicKey: 'abc123',
            signature: 'xyz',
            data: '+15504675309',
            verifiedBy: []
        },
        {
            id: 'idxcompanyname123',
            name: 'companyName',
            type: 'Text',
            publicKey: 'abc123',
            signature: 'xyz',
            data: 'Company A',
            verifiedBy: []
        }
    ];
}
function generateNameMap() : AliasMap {
    return {
        'idxlastname123': [{ alias: 'LastName' }], 
        'idxemailAddress2123': [{ alias: 'workEmail' }], 
        'idxphoneNumberWork123': [{ alias: 'phoneNumber' }], 
    };
}

test('Lens Data can be iterated', async () => {
    const originalData = generateData();
    const originalNameMap = generateNameMap();
    const root : LensData = new LensData(originalData, originalNameMap);

    let x=0;
    for(let lData of root) {
        expect(lData.value).toBe(originalData[x].data);
        x++;
    }
    expect(root.length).toBe(x);
});

test('Lens Data can be indexed', async () => {
    const originalData = generateData();
    const originalNameMap = generateNameMap();
    const root : LensData = new LensData(originalData, originalNameMap);

    // next() needs to work.
    expect(root.value).toBe(originalData[0].data);
    expect(root.next().value).toBe(originalData[1].data);
    expect(root.next().next().value).toBe(originalData[2].data);

    // index() needs to work.
    expect(root.index(originalData.length-1).value).toBe(originalData[originalData.length-1].data);

    // last() needs to work.
    expect(root.last().value).toBe(originalData[originalData.length-1].data);

    // next() past the last item returns null
    expect(root.last().next()).toBe(null);
});

test('Lens Data can be converted to an array', async ()=> {
    const originalData = generateData();
    const originalNameMap = generateNameMap();
    const root : LensData = new LensData(originalData, originalNameMap);

    const ldArray : Array<LensData> = root.toArray();
    expect(ldArray.length).toBe(root.length);
    expect(ldArray[0].length).toBe(root.length);
    expect(ldArray[1].length).toBe(root.length-1);
    expect(ldArray[2].length).toBe(root.length-2);
    expect(ldArray[3].length).toBe(root.length-3);

    expect(ldArray[0].value).toBe(root.index(0).value);
    expect(ldArray[1].value).toBe(root.index(1).value);
    expect(ldArray[2].value).toBe(root.index(2).value);
    expect(ldArray[2].next().value).toBe(root.index(2).next().value);
    expect(ldArray[2].next().value).toBe(root.index(3).value);
});

test('Lens Data can be filtered using "get"', async() => {
    const originalData = generateData();
    const originalNameMap = generateNameMap();
    const root : LensData = new LensData(originalData, originalNameMap);

    // Get should always return a copy
    expect(root.get()).not.toBe(root);

    // But results should be the same.
    expect(root.get().value).toBe(root.value);

    // Getting an item with only one name works how you expect.
    const firstName = root.get('firstName');
    expect(firstName.length).toBe(1);
    expect(firstName.value).toBe(originalData.filter(d => d.name == 'firstName')[0].data);

    // An item with a nameMap only works using the name map
    const lastName = root.get('LastName');
    expect(lastName.length).toBe(1);
    expect(lastName.value).toBe(originalData.filter(d => d.name == 'lastName')[0].data);
    expect(root.get('lastName')).toBe(null);

    // An item with two works as expected
    const phoneNumber = root.get('phoneNumber');
    expect(phoneNumber.length).toBe(2);
    expect(phoneNumber.value).toBe(originalData.filter(d => d.name == 'phoneNumber')[0].data);
    expect(phoneNumber.next().value).toBe(originalData.filter(d => d.name == 'phoneNumberWork')[0].data);

    // Items with aliases always use the alias.
    expect(root.get('emailAddress').length).toBe(1);
    expect(root.get('workEmail').length).toBe(1);

    // Getting something that doesn't exist returns null
    expect(root.get('nothing')).toBe(null);
});