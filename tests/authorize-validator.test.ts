import AuthorizeValidator from '../source/authorize-validator';
import { AuthorizeRequest } from '../source/types';
import ScopeType from '../source/scopeType';

test('A lens request normalizes', async () => {

    const request : AuthorizeRequest = {
        redirectUri: 'http://wherever',

        lensRequest: {data: [{ name:"test"}]}
    };

    const validator = new AuthorizeValidator();
    const newRequest = validator.normalize(request);

    expect(newRequest).not.toBe(request);
    expect(newRequest.redirectUri).toBe(request.redirectUri);
    expect(newRequest.scope.length).toBe(1);
    expect(newRequest.scope[0]).toBe(ScopeType.OpenId);
    expect(newRequest.lensRequest.data['name']).toBe(request.lensRequest.data['name']);

    const request2 : AuthorizeRequest = {
        redirectUri: 'http://wherever',
        scope: [ScopeType.OpenId, ScopeType.Identity],
        state: "something",

        lensRequest: {data: [{ name:"test"}]}
    };

    const newRequest2 = validator.normalize(request2);

    expect(newRequest2).not.toBe(request2);
    expect(newRequest2.redirectUri).toBe('http://wherever');
    expect(newRequest2.scope.length).toBe(2);
    expect(newRequest2.scope[0]).toBe(ScopeType.OpenId);
    expect(newRequest2.scope[1]).toBe(ScopeType.Identity);
    expect(newRequest2.state).toBe('something');
    expect(newRequest2.lensRequest.data['name']).toBe(request.lensRequest.data['name']);
});

test('A lens request validates', async () => {
    const request : AuthorizeRequest = {
        redirectUri: 'http://whatever',
        scope: [ScopeType.OpenId],
        state: 'someState',
        lensRequest: {
            data: [
                {
                    "name": "FirstName",
                    "type": "string",
                    "label": "First Name"
                },
                {
                    "name": "FamilyName",
                    "type": "string",
                    "label": "Last Name",
                    "display": "text",
                    "validators": [
                        "required",
                        { "type": "length", "min": 3 }
                    ]
                },
                {
                    "name": "HomeAddress",
                    "type": "string",
                    "label": "Home Address",
                    "display": "textarea",
                    "rows": 2
                },
                {
                    "name": "EmailAddress",
                    "type": "string/email-address",
                    "label": "Email Address",
                    "validators": [
                        { "type": "regex", "test": ".+@gmail[.]com", "label": "Must be a valid Gmail email address." }
                    ]
                },
                {
                    "name": "PhoneNumber",
                    "type": "string/phone-number",
                    "label": "Phone Number"
                },
                {
                    "name": "Country",
                    "type": "string",
                    "label": "Country of Residence",
                    "display": "select",
                    "options": [
                        { "value": "us", "label": "United States of America" },
                        { "value": "uk", "label": "United Kingdom" },
                        { "value": "ca", "label": "Canada" }
                    ],
                    "validators": [
                        { "type": "options", "label": "You must be a resident of the US, UK, or Canada." }
                    ]
                },
                {
                    "name": "bikeToWorkPerWeek",
                    "type": "number/integer",
                    "label": "How many times a week do you bike to work?",
                    "min": 0,
                    "max": 7
                },
                {
                    "name": "flightsPerYear",
                    "type": "number/integer",
                    "label": "How many flights do you take per year?",
                    "options": [
                        { "value": 0, "label": "I never fly" },
                        { "value": 1, "label": "A couple of times a year" },
                        { "value": 10, "label": "Once a month" },
                        { "value": 100, "label": "I fly every week or more" }
                    ],
                    "validators": [
                        { 
                            "type": "options",
                            "options": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 ], 
                            "label": "Only pick the right ones!" 
                        }
                    ]
                },
                {
                    "name": "isComposting",
                    "type": "boolean",
                    "label": "Do you compost?",
                    "display": "select",
                    "options": [
                        { "value": true, "label": "Yes" },
                        { "value": false, "label": "No" }
                    ]
                },
                {
                    "name": "isRecycling",
                    "type": "boolean",
                    "label": "Do you recycle?",
                    "display": "radio",
                    "options": [
                        { "value": true, "label": "Yes, all the time" },
                        { "value": false, "label": "No or rarely ever" }
                    ]
                },
                {
                    "name": "el_termsOfService",
                    "type": "boolean",
                    "label": "I agree to EarthLock's Terms of Service",
                    "display": "checkbox",
                    "validators": [
                        "required",
                        { "type": "options", "options": [true], "label": "You must agree to our Terms of Service" }
                    ]
                }
            ]
        }
    };

    const validator = new AuthorizeValidator();
    const newRequest = validator.normalize(request);
    validator.validate(newRequest);
})